App.MovieList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    keyObjects: null,
    keyObjectsFiltered: null,
    page: 0,
    itemPerPage: 15,
    genreList: null,

    filtering: false,
    initialize: function (options) {
        if (options && options.listTitle) {
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {},

    render: function () {

        //alert($(window).width());
        var self = this;
        $.ajax({
            url: "php/html/movieListView.php",
            method: "GET",
            dataType: "html",
        }).success(function (html) {

            $(self.el).append(html).promise()
                .done(function () {

                });

            $("#addBtn").on("click", function () {
                window.location = document.URL + "/new";
            });

            $.ajax({
                url: "api/dvd/getMovie.php",
                method: "GET",
                dataType: "json",
                cache: false,
                data: {getCount: true}
            }).success(function (json) {

                self.updateNavigation(json.data[0].totalNum);

                $.ajax({
                    url: "api/dvd/getGenreList.php",
                    method: "GET",
                    dataType: "json",
                    cache: false,
                    data: {}
                }).success(function (json) {
                    self.genreList = json.data;

                    $("#search").change(function () {
                    });
                    $('#search').on('keyup', function () {

                        if (this.value.length > 0) {
                            self.keyObjectsFiltered = searchStringInMovieTitle($("#search").val(), self.keyObjects);
                            $.each(self.keyObjects, function (index, obj) {
                                obj.filtered = false;
                            });

                            if (self.keyObjectsFiltered == 0) {
                                $("#itemContainer").empty();
                                $("#paginationContainer").hide();
                                $("#noResultMsg").show();
                            }
                            else {

                                $("#paginationContainer").show();
                                $("#noResultMsg").hide();

                                self.filtering = true;
                                $.each(self.keyObjects, function (index, obj) {

                                    if (self.keyObjectsFiltered.indexOf(obj) > -1) {
                                        obj.filtered = true;
                                    }
                                });

                                self.page = 0;
                                self.loadItem(self.page);
                            }
                        }
                        else {
                            $("#paginationContainer").show();
                            $("#noResultMsg").hide();
                            self.filtering = false;
                            self.page = 0;
                            self.loadItem(self.page);
                        }
                    });
                    self.loadItem(self.page);
                }).error(function (d) {
                    console.log('error');
                    console.log(d);
                });

            }).error(function (d) {
                console.log('error');
                console.log(d);
            });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });

    },
    updateNavigation: function (_items) {

        var self = this;
        $("#paginationContainer").pagination({
            items: _items,
            itemsOnPage: self.itemPerPage,
            cssStyle: 'light-theme',
            onPageClick: function (pageNum, event) {
                self.page = pageNum - 1;
                self.loadItem(pageNum - 1);
            }
        });
    },
    goBackToFirstPage: function () {
        $("#paginationContainer").pagination('selectPage', 1);
    },
    refresh: function () {
        var self = this;
        self.keyObjects = null;
        self.keyObjectsFiltered = null;
        self.loadItem(0);
    },
    loadItem: function (page) {

        var self = this;


        console.log("XXX"+page);

        $("#itemContainer").empty();

        if (self.keyObjects == null) {
            $.ajax({
                url: "api/dvd/getMovie.php",
                method: "GET",
                dataType: "json",
                cache: false,
                data: {}
            }).success(function (json) {

                self.keyObjects = json.data;

                $("#paginationContainer").pagination('updateItems', self.keyObjects.length);

                var upperLimit = self.keyObjects.length < self.itemPerPage ? self.keyObjects.length : self.itemPerPage;

                for (var x = page * self.itemPerPage; x < upperLimit * (page + 1) && x < self.keyObjects.length; x++) {

                    var isButtonClicked = false;
                    var itemRoot = document.createElement('div');
                    $(itemRoot).addClass("itemRoot");
                    itemRoot.value = x;

                    $("#itemContainer").append($(itemRoot));

                    $(itemRoot).append('<div id=' + 'name' + x + '></div>');
                    $("#name" + x).text(self.keyObjects[x].movieTitle);
                    $("#name" + x).addClass("name");

                    $(itemRoot).append('<div id=' + 'year' + x + '></div>');
                    $("#year" + x).text(self.keyObjects[x].year);
                    $("#year" + x).addClass("year");

                    $(itemRoot).append('<div id=' + 'genre' + x + '></div>');
                    $("#genre" + x).text(self.keyObjects[x].genre);
                    $("#genre" + x).addClass("genre");

                    $(itemRoot).append('<div id=' + 'stock' + x + '></div>');

                    $(itemRoot).append("<div id=borrow" + x + " class=\"borrow\" order=" + x + " title='click to borrow'" + "> " +
                        "<button type=\"button\" class=\"btn btn-default \" >" +
                        "<span class=\"glyphicon glyphicon-share\" aria-hidden=\"true\"></span>" + "Make Request" +
                        "</button>" +
                        "</div>");

                    $("#borrow" + x).on('click', function () {
                        var index = $(this).attr("order");
                        isButtonClicked = true;

                        App.addBorrowPopup = new App.AddBorrowPopup(
                            {
                                keyObj: self.keyObjects[index],
                                from: "movielist"
                            }
                        );
                    });

                    $(itemRoot).append("<div id=stockinfo" + x + " class=\"borrow\" order=" + x + " title='click to edit stock'" + "> " +
                        "<button type=\"button\" class=\"btn btn-default \" >" +
                        "<span class=\"glyphicon glyphicon-zoom-in\" aria-hidden=\"true\"></span>" + "Stock" +
                        "</button>" +
                        "</div>");

                    $("#stockinfo" + x).on('click', function () {
                        var index = $(this).attr("order");
                        isButtonClicked = true;

                        App.addInventoryPopup = new App.AddInventoryPopup(
                            {
                                keyObj: self.keyObjects[index]
                            }
                        );
                    });

                    var stockString = self.keyObjects[x].available + "/" + self.keyObjects[x].stock;

                    if (self.keyObjects[x].available == 0) {
                        $("#borrow" + x).css("visibility", "hidden");
                    }
                    $("#stock" + x).text(stockString);
                    $("#stock" + x).addClass("stock");

                    $(itemRoot).on('click', function () {
                        if (!isButtonClicked) {
                            window.location = document.URL + "/edit/" + self.keyObjects[this.value].movieId;
                        }
                    });
                }

                //enable the show tip box function for swap button (a request by Chris)
                $(".button-globe").tooltip();


            }).error(function (d) {
                console.log('error');
                console.log(d);
            });
        }
        else {

            var keyObj = self.filtering ? self.keyObjectsFiltered : self.keyObjects;

            var upperLimit = keyObj.length < self.itemPerPage ? keyObj.length : self.itemPerPage;

            $("#paginationContainer").pagination('updateItems', keyObj.length);

            for (var x = page * self.itemPerPage; x < upperLimit * (page + 1) && x < self.keyObjects.length; x++) {
                var isBorrowClicked = false;
                var itemRoot = document.createElement('div');
                $(itemRoot).addClass("itemRoot");
                itemRoot.value = x;

                $("#itemContainer").append($(itemRoot));

                $(itemRoot).append('<div id=' + 'name' + x + '></div>');
                $("#name" + x).text(keyObj[x].movieTitle);
                $("#name" + x).addClass("name");

                $(itemRoot).append('<div id=' + 'year' + x + '></div>');
                $("#year" + x).text(keyObj[x].year);
                $("#year" + x).addClass("year");

                $(itemRoot).append('<div id=' + 'genre' + x + '></div>');
                $("#genre" + x).text(keyObj[x].genreString);
                $("#genre" + x).addClass("genre");

                $(itemRoot).append('<div id=' + 'stock' + x + '></div>');

                $(itemRoot).append("<div id=borrow" + x + " class=\"borrow\" order=" + x + " title='click to borrow'" + "> " +
                    "<button type=\"button\" class=\"btn btn-default \" >" +
                    "<span class=\"glyphicon glyphicon-share\" aria-hidden=\"true\"></span>" + "Borrow" +
                    "</button>" +
                    "</div>");

                $("#borrow" + x).on('click', function () {
                    var index = $(this).attr("order");
                    isBorrowClicked = true;

                    App.addBorrowPopup = new App.AddBorrowPopup(
                        {
                            keyObj: keyObj[index],
                            from: "movielist"
                        }
                    );
                });

                $(itemRoot).append("<div id=stockinfo" + x + " class=\"borrow\" order=" + x + " title='click to edit stock'" + "> " +
                    "<button type=\"button\" class=\"btn btn-default \" >" +
                    "<span class=\"glyphicon glyphicon-zoom-in\" aria-hidden=\"true\"></span>" + "Stock" +
                    "</button>" +
                    "</div>");

                $("#stockinfo" + x).on('click', function () {
                    var index = $(this).attr("order");
                    isButtonClicked = true;

                    App.addInventoryPopup = new App.AddInventoryPopup(
                        {
                            keyObj: keyObj[index]
                        }
                    );
                });

                var stockString = keyObj[x].available + "/" + keyObj[x].stock;
                if (keyObj[x].available == 0) {
                    $("#borrow" + x).css("visibility", "hidden");
                }
                $("#stock" + x).text(stockString);
                $("#stock" + x).addClass("stock");

                $(itemRoot).on('click', function () {
                    if (!isBorrowClicked) {
                        window.location = document.URL + "/edit/" + keyObj[this.value].movieId;
                    }
                });
            }
        }
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {
        $("#itemListContainer").remove();
        this.undelegateEvents();
    },

    isHide: false
});