App.LatestList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    keyObjects:null,
    type: "",
    title: "Latest Movie",
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
    },

    render: function(){

        var self = this;
        $.ajax({
            url : "php/html/itemList.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){

            $(self.el).append(html).promise()
                .done(function() {
                    //alert("done");

                    if(self.type!=null) {
                        $("table").last().attr("id", "resultTable" + self.type);
                    }

                    self.postUISetup();
                    $("#boxer").after($("#itemListContainer"));

                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },

    postUISetup:function(){

        //we don't have search for itemList component
        $("#search").hide();

        //Insert Table Header
        var tableHeaderString = "<th class='tableHead'>Movie Title</th>"+
            "<th class='tableHead'>Remove From Latest</th>"+
            "<th class='tableHead'></th>";

        $("#resultTable"+this.type).append(
            "<tbody><tr>"+
            tableHeaderString+
            "</tr><tbody>");

        var self = this;

        $("#item_list_head_title").text(this.title);

        $('#addBtn').on('click',function(){
            App.addLatestPopup = new App.AddLatestPopup(
                {
                    root:self
                }
            );
        });

        //setup add btn text
        var addBtnText = self.title;
        $('#addBtn').text("+Add " + addBtnText);

        $.ajax({
            url : "api/dvd/getLatestList.php",
            method : "GET",
            dataType: "json",
            cache: false,
        }).success(function(json){
            //setTimeout(App.hideLoading, 1000);

            self.keyObjects =  json.data;

            for(var x = 0; x < self.keyObjects.length; x++){

                var firstColContent = "<tr id='"+self.keyObjects[x].id+"' style='height:30px'><td>"+"<div class ='titleContainer'></div>" +"<div class='titleText'>"+self.keyObjects[x].movieTitle+"</div></div>"+"</td>";
                var secondColContent = "<td>"+"<div id=remove" + x + " class=\"borrow\" order=" + x + " title='click to remove'" + "> " +
                    "<button type=\"button\" class=\"btn btn-default \" >" +
                    "<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span>" + "Remove" +
                    "</button>" +
                    "</div></td>";
                var thirdColContent = "<td><div class='swapBtn' title='drag it to swap the order.'>"+"</td>";

                $("#resultTable"+self.type).append(
                    firstColContent + secondColContent + thirdColContent+ "</tr>");

                $("#remove" + x).on('click', function () {
                    var index = $(this).attr("order");
                    App.yesNoPopup = new App.YesNoPopup(
                        {
                            yesFunc: function () {

                                $.ajax({
                                    url: "api/dvd/deleteLatest.php",
                                    method: "POST",
                                    dataType: "json",
                                    data: {
                                        id:  self.keyObjects[index].id,
                                    }
                                }).success(function (json) {
                                    console.log(json);

                                    if (json.status == 502) {
                                        alert(App.strings.sessionTimeOut);
                                        App.yesNoPopup.destroy();
                                        location.reload();
                                        return;
                                    }
                                    App.yesNoPopup.destroy();
                                    location.reload();
                                }).error(function (d) {
                                    console.log('error');
                                    console.log(d);
                                });

                            },
                            msg: "Are you sure to remove this movie?"
                        }
                    );
                });
            }

            //enable the show tip box function for swap button (a request by Chris)
            $( ".swapBtn" ).tooltip();

            console.log(App.idArray);
            console.log(App.typeArray);

            /*setup swapping function*/
            var fixHelper = function (e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function (index) {
                    // Set helper cell sizes to match the original sizes
                    $(this).width($originals.eq(index).width());
                });

                // append it to the body to avoid offset dragging
                $("#container").append($helper);

                return $helper;
            }

            var stopHelper = function(e, ui) {
                //alert("stop sorting");
                //alert( "Index: " + $("tr").index( $( "#140" )));
                console.log("total = " + self.keyObjects.length);

                var idArr = new Array();
                var orderArr = new Array();

                $.each( self.keyObjects, function( index, obj ){
                    console.log("obj.id = " + obj.id);

                    obj.order = $("tr").index($( "#"+obj.id));

                    console.log("obj.order = " + obj.order);

                    idArr.push(obj.id);
                    orderArr.push(obj.order-1);
                });

                $.ajax({
                    url : "api/dvd/updateItemOrder.php",
                    method : "POST",
                    dataType: "json",
                    data : {idArr:idArr.join(), orderArr:JSON.stringify(orderArr), type:"latest"}
                }).success(function(json){
                    console.log(json);
                    if(json.status == 502){
                        alert(App.strings.sessionTimeOut);
                        location.reload();
                        return;
                    }

                    if(json.status == 1){
                        console.log("The order is saved.");
                        App.showTipBox("suscess-mode","Suscess","The order is saved.");
                    }

                }).error(function(d){
                    console.log('error');
                    console.log(d);
                    App.showTipBox("fail-mode","Failed","Please try again later");

                });

                return ui;
            }

            $('#resultTable  tbody').sortable({
                helper: fixHelper,
                axis: 'y',
                stop: stopHelper
            }).disableSelection();

            /*$('.headRow').sortable({
                helper: fixHelper,
                axis: 'y'
            }).disableSelection();*/

           // $('.headRow').sortable( "disable" );
            $('#resultTable  tbody').sortable( "disable" );

            $('.swapBtn').on('mouseover',function(){
                $('#resultTable tbody').sortable( "enable" );
            });
            $('.swapBtn').on('mouseout',function(){
                $('#resultTable tbody').sortable( "disable" );
            });

            //if there is not more than one row, we dont need swap Button
            if(json.data.length <= 1){
                $('.swapBtn').hide();
                $('.optionSwapBtn').show();
            }

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        $("#item_list_boxer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    refresh: function(){
        var self = this;
        self.keyObjects = null;
        $(self.el).empty();
        self.render();
    },
    isHide : false
});