var refreshIntervalId = '';


App.RequestList = Backbone.View.extend({

    el: '#content_container',
    keyObjects: null,
    searchDate: '',
    searchStatus: -1,
    searchRoom: '',
    isDefault: true,
    scrollPos: 0,
    dataSize: 0,

    initialize: function (options) {
        if (options && options.searchDate) {
            this.searchDate = options.searchDate;
            this.isDefault = false;
        } else {
            this.searchDate = new Date().toJSON().slice(0, 10).replace(/-/g, '-');
        }
        if (options && options.searchStatus) {
            this.searchStatus = options.searchStatus;
            this.isDefault = false;
        }
        if (options && options.searchRoom) {
            this.searchRoom = options.searchRoom;
            this.isDefault = false;
        }
        this.render();
    },

    events: {},

    render: function () {

        var self = this;
        $.ajax({
            url: "php/html/requestListView.php",
            method: "GET",
            dataType: "html",
        }).success(function (html) {

            $(self.el).append(html).promise().done(function () {
            });

            function splitParam(str) {
                var i = str.indexOf("?");
                if (i > 0)
                    return str.slice(0, i);
                else
                    return str;
            }

            if(self.isDefault){
                $("#list_head_title").html("All Pending Record (No Filter)");
            } else {
                $("#list_head_title").html("All Filtered Record");
            }

            $("#dateInput").val(self.searchDate);
            $("#statusSelectionbox").val(self.searchStatus);
            $("#roomInput").val(self.searchRoom);

            $("#dateInput").datepicker({dateFormat: 'yy-mm-dd',});

            $('#searchBtn').on('click', function () {
                self.searchDate = $('#dateInput').val();
                self.searchStatus = $('#statusSelectionbox').val();
                self.searchRoom = $('#roomInput').val();

                var searchDateString = '?date=' + $('#dateInput').val();
                var searchStatusString = '';
                var searchRoomString = '';

                if ($('#statusSelectionbox').val() >= 0) {
                    searchStatusString = '&status=' + $('#statusSelectionbox').val();
                }

                if ($('#roomInput').val().toString().length > 0) {
                    searchStatusString = '&room=' + $('#roomInput').val();
                }
                window.location = splitParam(document.URL) + searchDateString + searchStatusString + searchRoomString;
            });
            self.loadItem();


        }).error(function (d) {
            console.log('error');
            console.log(d);
        });

    },
    refresh: function () {
        var self = this;
        self.keyObjects = null;
        self.loadItem();
    },
    loadItem: function () {

        var self = this;
        var pendingAlert = false;
        var audioFile = "media/bell.mp3";

        clearInterval(refreshIntervalId);

        refreshIntervalId = window.setInterval(function () {
            if (App.requestList != null) {
                self.scrollPos = $(document).scrollTop();
				$.ajax({
					url: apiUrl,
					method: "GET",
					cache: false,
					dataType: "json",
					data: {
						date: self.searchDate,
						status: self.searchStatus,
						room: self.searchRoom
					}
				}).success(function (json) {
					self.keyObjects = json.data;
					if(self.dataSize != self.keyObjects.length) {
						self.dataSize = 0;
						App.requestList.refresh();
					}
				});
            } else {
                clearInterval(refreshIntervalId);
                refreshIntervalId = '';
            }
        }, 5000);

        function playSound() {
            console.log("playSound");
            if (App.requestList != null) {
                new Audio(audioFile).play();
            }
        };

        $("#itemContainer").empty();

        var apiUrl = "api/dvd/getRequestList.php";

        if(self.isDefault){
            apiUrl = "api/dvd/getDefaultRequestList.php"
        }

        $.ajax({
            url: apiUrl,
            method: "GET",
            cache: false,
            dataType: "json",
            data: {
                date: self.searchDate,
                status: self.searchStatus,
                room: self.searchRoom
            }
        }).success(function (json) {
            self.keyObjects = json.data;

            for (var x = self.dataSize; x < self.keyObjects.length; x++) {
                $.ajax({
                    url: "php/html/requestListItemView.php",
                    method: "GET",
                    dataType: "html",
                    async: false,
                    data: {order: x}
                }).success(function (html) {

                        $("#itemContainer").append(html).promise()
                            .done(function () {
                            });

                        $("#room" + x).text(self.keyObjects[x].room);
                        $("#requesttime" + x).text(self.keyObjects[x].requesttime);
                        $("#title" + x).text(self.keyObjects[x].title);

                        var isProcessClickable = false;
                        var isReturnClickable = false;
                        var isCancelShow = false;

                        if (self.keyObjects[x].statusId == 0) {
                            $("#status" + x).text("Available");
                        } else if (self.keyObjects[x].statusId == 1) {
                            pendingAlert = true;
                            $("#status" + x).text("Waiting for process");
                            isProcessClickable = true;
                            isCancelShow = true;
                        } else if (self.keyObjects[x].statusId == 2) {
                            $("#status" + x).text("In use");
                            isReturnClickable = true;
                        } else if (self.keyObjects[x].statusId == 3) {
                            pendingAlert = true;
                            $("#status" + x).text("Waiting for return");
                            isReturnClickable = true;
                        } else if (self.keyObjects[x].statusId == 4) {
                            $("#status" + x).text("Returned");
                        } else if (self.keyObjects[x].statusId == 5) {
                            $("#status" + x).text("Cancelled");
                        } else if (self.keyObjects[x].statusId == 6) {
                            pendingAlert = true;
                            $("#status" + x).text("Booked");
                            isProcessClickable = true;
                            isCancelShow = true;
                        } else {
	                        $("#status" + x).text("Checkout - " + x);
                        }
			if (self.keyObjects[x].statusId == 12 || self.keyObjects[x].statusId == 13) {
			    pendingAlert = true;
			    isReturnClickable = true;
			}

                        $("#assetId" + x).text(self.keyObjects[x].assetId);
                        $("#updatetime" + x).text(self.keyObjects[x].lastupdate);
                        $("#updatedby" + x).text(self.keyObjects[x].lastupdateby);

                        if (isCancelShow) {
                            $("#cancelBtn" + x).show();
                            $("#cancelBtn" + x).on('click', function () {
                                var index = $(this).attr("order");
                                App.yesNoPopup = new App.YesNoPopup(
                                    {
                                        yesFunc: function () {

                                            $.ajax({
                                                url: "api/dvd/updateRequest.php",
                                                method: "POST",
                                                dataType: "json",
                                                data: {
                                                    requestId: self.keyObjects[index].id,
                                                    statusId: 5
                                                }
                                            }).success(function (json) {
                                                App.yesNoPopup.destroy();
                                                location.reload();
                                            }).error(function (d) {
                                                App.yesNoPopup.destroy();
                                                console.log('error');
                                                console.log(d);
                                            });
                                        },
                                        msg: "Are you sure to cancel this request?"
                                    }
                                );
                            });
                        } else {
                            $("#cancelBtn" + x).hide();
                        }

                        if (isProcessClickable) {
                            $("#processBtn" + x).addClass("request_item_green_btn_on");
                            $("#processBtn" + x).on('click', function () {
                                var index = $(this).attr("order");
                                var updatedStatusId = '';

                                if (self.keyObjects[index].statusId == 1) {

                                    App.addBorrowPopup = new App.AddBorrowPopup(
                                        {
                                            keyObj: self.keyObjects[index],
                                            from: "requestlist"
                                        }
                                    );
                                } else {
                                    if (self.keyObjects[index].statusId == 3) {
                                        updatedStatusId = 4;
                                    } else if (self.keyObjects[index].statusId == 6) {
                                        updatedStatusId = 1;
                                    }

                                    $.ajax({
                                        url: "api/dvd/updateRequest.php",
                                        method: "POST",
                                        dataType: "json",
                                        data: {
                                            requestId: self.keyObjects[index].id,
                                            statusId: updatedStatusId
                                        }
                                    }).success(function (json) {
                                        self.dataSize=0;
                                        App.requestList.refresh();
                                    }).error(function (d) {
                                        console.log(d);
                                    });
                                }
                            });
                        } else {
                            $("#processBtn" + x).addClass("request_item_btn_off");
                        }

                        if (isReturnClickable) {

                            if (self.keyObjects[x].statusId == 2) {
                                $("#returnBtn" + x).addClass("request_item_yellow_btn_on");
                                $("#returnBtn" + x).text('Manual Return');
                            } else {
                                $("#returnBtn" + x).addClass("request_item_green_btn_on");
                            }

                            $("#returnBtn" + x).on('click', function () {
                                var index = $(this).attr("order");
                                var updatedStatusId = '';

                                if (self.keyObjects[index].statusId == 1) {
                                    updatedStatusId = 2;
                                } else if (self.keyObjects[index].statusId == 2) {
                                    updatedStatusId = 4;
                                } else if (self.keyObjects[index].statusId == 3) {
                                    updatedStatusId = 4;
                                } else if (self.keyObjects[index].statusId == 6) {
                                    updatedStatusId = 1;
                                }
                                if (self.keyObjects[index].statusId == 12 || self.keyObjects[index].statusId == 13) {
                                    updatedStatusId = 14;
				}

                                $.ajax({
                                    url: "api/dvd/updateRequest.php",
                                    method: "POST",
                                    dataType: "json",
                                    data: {
                                        requestId: self.keyObjects[index].id,
                                        statusId: updatedStatusId
                                    }
                                }).success(function (json) {
                                    self.dataSize=0;
                                    App.requestList.refresh();
                                }).error(function (d) {
                                    console.log(d);
                                });
                            });
                        }
                        else {
                            $("#returnBtn" + x).addClass("request_item_btn_off");
                        }

                        $("#printBtn" + x).on('click', function () {
                            var index = $(this).attr("order");
                            var updatedStatusId = '';
                            self.print(index);
                        });

                    }
                ).error(function (d) {
                    console.log(d);
                });

            }
            self.dataSize=self.keyObjects.length;
            if (pendingAlert) {
                playSound();
                $("#positiveBtn").effect("shake");
            }
            $(document).scrollTop(self.scrollPos);
        }).error(function (d) {
            console.log('error');
            console.log(d);
        });

    },
    close: function () {
        console.log("close fire");
    }
    ,
    destroy: function () {
        console.log("destroy");
        $("#itemListContainer").remove();
        this.undelegateEvents();
    },

    print: function PrintPreview(id) {

        var self = this;

        $.ajax({
            url: "php/html/printRequestListItemView.php",
            method: "GET",
            dataType: "html",
            async: false,
            data: {order: id}
        }).success(function (html) {

                var x = id;
                var statusString = "";
                if (self.keyObjects[x].statusId == 0) {
                    statusString = "Available";
                } else if (self.keyObjects[x].statusId == 1) {
                    statusString = "Waiting for process";
                } else if (self.keyObjects[x].statusId == 2) {
                    statusString = "In use";
                } else if (self.keyObjects[x].statusId == 3) {
                    statusString = "Waiting for return";
                } else if (self.keyObjects[x].statusId == 4) {
                    statusString = "Returned";
                } else if (self.keyObjects[x].statusId == 5) {
                    statusString = "Cancelled";
                } else if (self.keyObjects[x].statusId == 6) {
                    statusString = "Booked";
                }

                var popupWin = window.open('', '_blank');
                popupWin.document.open();
                popupWin.document.write(html);
                popupWin.document.getElementById("room").textContent = self.keyObjects[x].room;
                popupWin.document.getElementById("requesttime").textContent = self.keyObjects[x].requesttime;
                popupWin.document.getElementById("title").textContent = self.keyObjects[x].title;
                popupWin.document.getElementById("assetId").textContent = self.keyObjects[x].assetId;
                popupWin.document.getElementById("updatetime").textContent = self.keyObjects[x].lastupdate;
                popupWin.document.getElementById("updatedby").textContent = self.keyObjects[x].lastupdateby;
                popupWin.document.getElementById("status").textContent = statusString;
                popupWin.document.close();
            }
        ).error(function (d) {
            console.log(d);
        });

    },
    isHide: false
})
;


