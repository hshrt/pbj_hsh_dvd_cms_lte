App.MovieDetail = Backbone.View.extend({

    el: '#content_container',
    uploadImageSizeLimit: 5,
    posterWidth: 270,
    posterHeight: 400,

    currentLang: "en",
    jsonObj: null,

    type: null,
    movieId: null,
    titleLangModel: {},
    desLangModel: {},

    genreList: [],
    selected_genre: [],

    languageList: [],
    selected_language: [],

    subtitleList: [],
    selected_subtitle: [],

    divisionList: [],
    selected_division: "",

    rating: 0,

    posterFilePath: null,
    year: null,

    isNewPoster: false,

    title: "Movie",

    initialize: function (options) {
        this.selectGuestModel = [];
        if (options && options.id) {
            this.movieId = options.id;
        }
        if (options && options.type) {
            this.type = options.type;
        }
        this.render();
    },
    events: {
        'click #closeBtn_start': 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {

        var self = this;

        //remove any cached data if it is for new message
        if (self.type == "new") {
            App.currentItem = [];
            App.currentId = "";
            self.selected_genre = [];
            self.selected_language = [];
            self.selected_subtitle = [];
            self.titleLangModel = [];
            self.desLangModel = [];
            self.selected_division = "";
            self.rating = 0;
        } else {
            App.currentItem = [];
            App.currentId = this.movieId;
        }

        $.ajax({
            url: "php/html/movieDetail.php",
            method: "GET",
            dataType: "html",
            data: {}
        }).success(function (html) {
            $(self.el).append(html).promise()
                .done(function () {

                    CKEDITOR.replace('editor1');

                    CKEDITOR.replace('editor2', {
                        on: {
                            focus: function () {
                                console.log("focus editor2")
                            },
                            blur: function () {
                                self.saveDescriptionLang()
                            }
                        }
                    });

                    //this is editing existed message
                    if (self.type != "new") {

                        $.ajax({
                            url: "api/dvd/getMovie.php",
                            method: "GET",
                            cache: false,
                            dataType: "json",
                            data: {movieId: App.currentId}
                        }).success(function (json) {
                            App.currentItem = json.data[0];
                            self.getSelectedGerne(self);
                            self.getSelectedLanguage(self);
                            self.getSelectedSubtitle(self);
                            self.getSelectedDivision(self);
                            self.getRating(self);
                            self.posterFilePath = App.currentItem.poster;
                            self.year = App.currentItem.year;
                            self.setupUIHandler();

                            //get language info for item Title
                            $.ajax({
                                url: "api/dvd/getLanguageMap.php",
                                method: "POST",
                                dataType: "json",
                                data: {id: App.currentItem.titleId}
                            }).success(function (json) {
                                console.log(json.data[0]);
                                self.titleLangModel = json.data[0];
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get language info for item Description
                            $.ajax({
                                url: "api/dvd/getLanguageMap.php",
                                method: "POST",
                                dataType: "json",
                                data: {id: App.currentItem.descriptionId}
                            }).success(function (json) {
                                console.log(json.data[0]);
                                self.desLangModel = json.data[0];
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get genre list
                            $.ajax({
                                url: "api/dvd/getGenreList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.genreList = json.data;
                                self.initGenreList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get language list
                            $.ajax({
                                url: "api/dvd/getLanguageList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.languageList = json.data;
                                self.initLanguageList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get subtitle list
                            $.ajax({
                                url: "api/dvd/getSubtitleList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.subtitleList = json.data;
                                self.initSubtitleList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get division list
                            $.ajax({
                                url: "api/dvd/getDivisionList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.divisionList = json.data;
                                self.initDivisionList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            self.initRating(self);

                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    }
                    else {//this is for new message
                        self.setupUIHandler();

                        //get genre list
                        $.ajax({
                            url: "api/dvd/getGenreList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.genreList = json.data;
                            self.initGenreList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get language list
                        $.ajax({
                            url: "api/dvd/getLanguageList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.languageList = json.data;
                            self.initLanguageList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get subtitle list
                        $.ajax({
                            url: "api/dvd/getSubtitleList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.subtitleList = json.data;
                            self.initSubtitleList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get division list
                        $.ajax({
                            url: "api/dvd/getDivisionList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.divisionList = json.data;
                            self.initDivisionList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        self.initRating(self);
                    }

                    setTimeout(function () {
                        var editor2 = CKEDITOR.instances.editor2;
                        editor2.setReadOnly(true);
                    }, 1000);

                });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function () {
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library" ? "Page" : self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function () {
        window.parent.PeriEvent.backToIndex();
    },
    setupUIHandler: function () {
        var self = this;

        //pre-fill the form
        self.type != "new" ? $('#itemName').text("Edit Movie") : $('#itemName').text("New Movie");

        if (self.type != "new") {
            self.loadPhoto();
            self.loadYear();
        }

        $('#container_en input').val(App.currentItem.movieTitle);
        $('#container_lang input').val(App.currentItem.movieTitle);

        var editor_en = CKEDITOR.instances.editor1;
        var editor_lang = CKEDITOR.instances.editor2;

        setTimeout(function () {
            var string = "";

            if (self.desLangModel.en.indexOf("<br />") == -1 && App.currentItem.lastUpdateBy == "TC3") {
                string = self.desLangModel.en.replace(/\n/g, "<br />");
            }
            else {
                string = self.desLangModel.en;
            }
            editor_en.setData(string);
        }, 1000);


        if (self.type == "new") {
            $("#delete_btn").hide();
        } else {
            //get count
            $.ajax({
                url: "api/dvd/getCount.php",
                method: "GET",
                dataType: "json",
                data: {type: "movie",
                        id: self.movieId
                }
            }).success(function (json) {
                if(json.data[0].totalNum > 0){
                    $("#delete_btn").hide();
                }
            }).error(function (d) {
                console.log('error');
                console.log(d);
            });

        }

        //handle delete button
        $('#delete_btn').on('click', function () {
            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc: function () {
                        $.ajax({
                            url: "api/dvd/deleteMovie.php",
                            method: "POST",
                            dataType: "json",
                            data: {id: App.currentId}
                        }).success(function (json) {
                            if (json.status == 502) {
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }
                            App.yesNoPopup.destroy();
                            App.goUpper2Level();

                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    },
                    msg: "Are you sure to delete this movie?"
                }
            );
        });

        //handle save button
        $('#save_btn').on('click', function () {

            if ($('#detailTitle_en').val() != null && $('#detailTitle_en').val().length < 1) {
                alert("Please input Eng Title .");
                return;
            }

            if ( editor_en.getData() != null &&  editor_en.getData().length < 1) {
                alert("Please input Eng Description .");
                return;
            }

            if (($("#yearInput").val()) == null || ($("#yearInput").val()).length < 1) {
                alert("Please fill in Year.");
                return;
            }

            if (self.selected_genre[0] == null) {
                alert("Please select at least one genre.");
                return;
            }

            if (self.selected_language[0] == null) {
                alert("Please select at least one language.");
                return;
            }

            if (self.selected_subtitle[0] == null) {
                alert("Please select at least one subtitle.");
                return;
            }

            if (self.type == "new") {
                if (!App.imageData || !App.imageFileName) {
                    alert("Please upload a poster first.");
                    return;
                }
            }

            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc: function () {
                        App.showLoading();

                        //save the description for other language first
                        self.saveDescriptionLang();

                        //updateItem exist message
                        if (self.type != "new") {
                            //updateItem Title of Item

                            var selectedLanguageList = '';
                            var selectedSubtitleList = '';
                            var selectedGenreList = '';

                            $.ajax({
                                url: "api/dvd/updateDict.php",
                                method: "POST",
                                dataType: "text",
                                data: {
                                    title_id: App.currentItem.titleId,
                                    title_en: $('#detailTitle_en').val(),
                                    title_zh_hk: self.titleLangModel.zh_hk,
                                    title_zh_cn: self.titleLangModel.zh_cn,
                                    title_jp: self.titleLangModel.jp,
                                    title_fr: self.titleLangModel.fr,
                                    title_ar: self.titleLangModel.ar,
                                    title_es: self.titleLangModel.es,
                                    title_de: self.titleLangModel.de,
                                    title_ko: self.titleLangModel.ko,
                                    title_ru: self.titleLangModel.ru,
                                    title_pt: self.titleLangModel.pt
                                }
                            }).success(function (json) {

                                console.log(json);
                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

                                //updateItem Description of Item
                                $.ajax({
                                    url: "api/dvd/updateDict.php",
                                    method: "POST",
                                    dataType: "text",
                                    data: {
                                        title_id: App.currentItem.descriptionId,
                                        title_en: editor_en.getData(),
                                        title_zh_hk: self.desLangModel.zh_hk,
                                        title_zh_cn: self.desLangModel.zh_cn,
                                        title_jp: self.desLangModel.jp,
                                        title_fr: self.desLangModel.fr,
                                        title_ar: self.desLangModel.ar,
                                        title_es: self.desLangModel.es,
                                        title_de: self.desLangModel.de,
                                        title_ko: self.desLangModel.ko,
                                        title_ru: self.desLangModel.ru,
                                        title_pt: self.desLangModel.pt
                                    }
                                }).success(function (json) {


                                    if (App.movieDetail.isNewPoster) {


                                        App.imageData = $('#imageContainer > img').cropper("getDataURL", { width:App.movieDetail.posterWidth , height: App.movieDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

                                        $.ajax({
                                            url: "api/dvd/uploadPhoto.php",
                                            method: "POST",
                                            dataType: "json",
                                            data: {image: App.imageData}
                                        }).success(function (json) {
                                            console.log(json);

                                            if (json.status == 502) {
                                                alert(App.strings.sessionTimeOut);
                                                location.reload();
                                                return;
                                            }

                                            self.posterFilePath = json.data.filepath;

                                            for (var x = 0; x < self.selected_genre.length; x++) {
                                                if (x != self.selected_genre.length - 1) {
                                                    selectedGenreList += self.selected_genre[x] + ",";
                                                }
                                                else {
                                                    selectedGenreList += self.selected_genre[x];
                                                }
                                            }

                                            for (var x = 0; x < self.selected_language.length; x++) {
                                                if (x != self.selected_language.length - 1) {
                                                    selectedLanguageList += self.selected_language[x] + ",";
                                                }
                                                else {
                                                    selectedLanguageList += self.selected_language[x];
                                                }
                                            }

                                            for (var x = 0; x < self.selected_subtitle.length; x++) {
                                                if (x != self.selected_subtitle.length - 1) {
                                                    selectedSubtitleList += self.selected_subtitle[x] + ",";
                                                }
                                                else {
                                                    selectedSubtitleList += self.selected_subtitle[x];
                                                }
                                            }

                                            $.ajax({
                                                url: "api/dvd/updateMovie.php",
                                                method: "POST",
                                                dataType: "json",
                                                data: {
                                                    movieId: App.currentId,
                                                    genreId: selectedGenreList,
                                                    year: $("#yearInput").val(),
                                                    posterurl: self.posterFilePath,
                                                    language: selectedLanguageList,
                                                    subtitle: selectedSubtitleList,
                                                    rating: parseFloat($("#ratingInput").val()).toFixed(1),
                                                    divisionId: $('#divisionSelector').find(":selected").val(),
                                                }
                                            }).success(function (json) {
                                                console.log(json);
                                                App.goUpper2Level();
                                            }).error(function (d) {
                                                console.log('error');
                                                console.log(d);
                                            });
                                            // }


                                        }).error(function (d) {
                                            console.log('error');
                                            console.log(d);
                                            alert("Upload photo error, please try again.");
                                            self.destroy();
                                        });
                                    } else {
                                        //updateItem Description of Item

                                        for (var x = 0; x < self.selected_genre.length; x++) {
                                            if (x != self.selected_genre.length - 1) {
                                                selectedGenreList += self.selected_genre[x] + ",";
                                            }
                                            else {
                                                selectedGenreList += self.selected_genre[x];
                                            }
                                        }

                                        for (var x = 0; x < self.selected_language.length; x++) {
                                            if (x != self.selected_language.length - 1) {
                                                selectedLanguageList += self.selected_language[x] + ",";
                                            }
                                            else {
                                                selectedLanguageList += self.selected_language[x];
                                            }
                                        }

                                        for (var x = 0; x < self.selected_subtitle.length; x++) {
                                            if (x != self.selected_subtitle.length - 1) {
                                                selectedSubtitleList += self.selected_subtitle[x] + ",";
                                            }
                                            else {
                                                selectedSubtitleList += self.selected_subtitle[x];
                                            }
                                        }

                                        $.ajax({
                                            url: "api/dvd/updateMovie.php",
                                            method: "POST",
                                            dataType: "json",
                                            data: {
                                                movieId: App.currentId,
                                                genreId: selectedGenreList,
                                                year: $("#yearInput").val(),
                                                posterurl: self.posterFilePath,
                                                language: selectedLanguageList,
                                                subtitle: selectedSubtitleList,
                                                rating: $("#ratingInput").val(),
                                                divisionId: $('#divisionSelector').find(":selected").val(),
                                            }
                                        }).success(function (json) {
                                            App.goUpper2Level();
                                        }).error(function (d) {
                                            console.log('error');
                                            console.log(d);
                                        });
                                        // }
                                    }


                                }).error(function (d) {
                                    console.log('error');
                                    console.log(d);
                                });

                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });
                            App.hideLoading();

                        }
                        //add mew message
                        else {

                            var selectedGenreList = '';
                            var selectedLanguageList = '';
                            var selectedSubtitleList = '';


                            App.imageData = $('#imageContainer > img').cropper("getDataURL", { width:App.movieDetail.posterWidth , height: App.movieDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

                            $.ajax({
                                url: "api/dvd/uploadPhoto.php",
                                method: "POST",
                                dataType: "json",
                                data: {image: App.imageData}
                            }).success(function (json) {
                                console.log(json);

                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

                                self.posterFilePath = json.data.filepath;

                                for (var x = 0; x < self.selected_genre.length; x++) {
                                    if (x != self.selected_genre.length - 1) {
                                        selectedGenreList += self.selected_genre[x] + ",";
                                    }
                                    else {
                                        selectedGenreList += self.selected_genre[x];
                                    }
                                }

                                for (var x = 0; x < self.selected_language.length; x++) {
                                    if (x != self.selected_language.length - 1) {
                                        selectedLanguageList += self.selected_language[x] + ",";
                                    }
                                    else {
                                        selectedLanguageList += self.selected_language[x];
                                    }
                                }

                                for (var x = 0; x < self.selected_subtitle.length; x++) {
                                    if (x != self.selected_subtitle.length - 1) {
                                        selectedSubtitleList += self.selected_subtitle[x] + ",";
                                    }
                                    else {
                                        selectedSubtitleList += self.selected_subtitle[x];
                                    }
                                }

                                $.ajax({
                                    url: "api/dvd/addMovie.php",
                                    method: "POST",
                                    dataType: "json",
                                    data: {
                                        genreId: selectedGenreList,
                                        language: selectedLanguageList,
                                        subtitle: selectedSubtitleList,
                                        year: $("#yearInput").val(),
                                        poster: self.posterFilePath,
                                        rating: $("#ratingInput").val(),
                                        divisionId: $('#divisionSelector').find(":selected").val(),

                                        title_en: $('#detailTitle_en').val(),
                                        title_zh_hk: self.titleLangModel.zh_hk,
                                        title_zh_cn: self.titleLangModel.zh_cn,
                                        title_jp: self.titleLangModel.jp,
                                        title_fr: self.titleLangModel.fr,
                                        title_ar: self.titleLangModel.ar,
                                        title_es: self.titleLangModel.es,
                                        title_de: self.titleLangModel.de,
                                        title_ko: self.titleLangModel.ko,
                                        title_ru: self.titleLangModel.ru,
                                        title_pt: self.titleLangModel.pt,

                                        description_en: editor_en.getData(),
                                        description_zh_hk: self.desLangModel.zh_hk,
                                        description_zh_cn: self.desLangModel.zh_cn,
                                        description_jp: self.desLangModel.jp,
                                        description_fr: self.desLangModel.fr,
                                        description_ar: self.desLangModel.ar,
                                        description_es: self.desLangModel.es,
                                        description_de: self.desLangModel.de,
                                        description_ko: self.desLangModel.ko,
                                        description_ru: self.desLangModel.ru,
                                        description_pt: self.desLangModel.pt
                                    }
                                }).success(function (json) {
                                    if (json.status == 502) {
                                        alert(App.strings.sessionTimeOut);
                                        location.reload();
                                        return;
                                    }
                                    console.log(json);
                                    App.goUpperLevel();
                                }).error(function (d) {
                                    console.log(d);
                                });

                            }).error(function (d) {
                                console.log(d);
                                alert("Upload photo error, please try again.");
                                App.goUpperLevel();
                            });
                        }
                        App.yesNoPopup.destroy();
                    },
                    msg: "Are you sure to add/edit movie?"
                }
            );


        });

        //handle language select box
        $("#lang_selectionBox").change(function () {

            var editor_lang = CKEDITOR.instances.editor2;

            if ($("#lang_selectionBox").val() != "") {
                self.currentLang = $("#lang_selectionBox").val();
            }

            //setup enable/disable the other language input box
            if ($("#lang_selectionBox").val() == 'en') {
                $('#container_lang input').attr('disabled', 'disabled');
                editor_lang.setReadOnly(true);
            }
            else {
                //alert("enable");
                $('#container_lang input').removeAttr('disabled');
                editor_lang.setReadOnly(false);
            }

            $('#container_lang input').val(eval("self.titleLangModel." + $("#lang_selectionBox").val()));

            var string = "";
            if (self.desLangModel.en.indexOf("<br />") == -1) {
                string = eval("self.desLangModel." + $("#lang_selectionBox").val()).replace(/\n/g, "<br />");
                //string  = string.replace(/\r/g, "<br />");
            }
            else {
                string = eval("self.desLangModel." + $("#lang_selectionBox").val());
            }
            //string = string.replace(/\r/g, "<br />");

            editor_lang.setData(string);
        });

        $('#container_lang input').blur(function () {
            var editor_lang = CKEDITOR.instances.editor2;
            //alert("hi");
            eval("self.titleLangModel." + $("#lang_selectionBox").val() + "= $('#container_lang input').val()");

            console.log(self.titleLangModel);
        });


        $("#takePictureField").on("change", self.gotPic);

    },

    getSelectedGerne: function (_self) {
        var self = _self;
        var selected_genre = App.currentItem.genreId;
        if (selected_genre != null && selected_genre.length > 0) {
            self.selected_genre = selected_genre.split(',');
        } else {
            self.selected_genre[0] = null;
        }
    },

    getSelectedLanguage: function (_self) {
        var self = _self;
        var selected_language = App.currentItem.languageId;
        if (selected_language != null && selected_language.length > 0) {
            self.selected_language = selected_language.split(',');
        } else {
            self.selected_language[0] = null;
        }
    },

    getSelectedSubtitle: function (_self) {
        var self = _self;
        var selected_subtitle = App.currentItem.subtitleId;
        if (selected_subtitle != null && selected_subtitle.length > 0) {
            self.selected_subtitle = selected_subtitle.split(',');
        } else {
            self.selected_subtitle[0] = null;
        }
    },

    getSelectedDivision: function (_self) {
        var self = _self;
        self.selected_division = App.currentItem.divisionId;
    },

    getRating: function (_self) {
        var self = _self;
        self.rating = App.currentItem.rating;
    },

    initGenreList: function (_self) {
        var self = _self;

        $("#genre_list").empty();

        var str = "";
        for (var x = 0; x < self.genreList.length; x++) {
            var isChecked = false;
            for (var y = 0; y < self.selected_genre[y]; y++) {
                if (self.genreList[x].id == self.selected_genre[y]) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                str = str + "<label><input type='checkbox' id=genreCheckbox" + x + " value='" + self.genreList[x].id + "' order=" + x + " checked />" + self.genreList[x].en + "</label>";
            } else {
                str = str + "<label><input type='checkbox' id=genreCheckbox" + x + " value='" + self.genreList[x].id + "' order=" + x + " />" + self.genreList[x].en + "</label>";
            }
        }

        $("#genre_list").html(str);
        self.initGenreCheckBox(self, $("#genre_list"));
    },

    initLanguageList: function (_self) {
        var self = _self;

        $("#language_list").empty();

        var str = "";
        for (var x = 0; x < self.languageList.length; x++) {
            var isChecked = false;

            for (var y = 0; y < self.selected_language[y]; y++) {
                if (self.languageList[x].id == self.selected_language[y]) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                str = str + "<label><input type='checkbox' id=languageCheckbox" + x + " value='" + self.languageList[x].id + "' order=" + x + " checked />" + self.languageList[x].en + "</label>";
            } else {
                str = str + "<label><input type='checkbox' id=languageCheckbox" + x + " value='" + self.languageList[x].id + "' order=" + x + " />" + self.languageList[x].en + "</label>";
            }
        }

        $("#language_list").html(str);
        self.initLanguageCheckBox(self, $("#language_list"));
    },


    initSubtitleList: function (_self) {
        var self = _self;

        $("#subtitle_list").empty();

        var str = "";
        for (var x = 0; x < self.subtitleList.length; x++) {
            var isChecked = false;

            for (var y = 0; y < self.selected_subtitle[y]; y++) {
                if (self.subtitleList[x].id == self.selected_subtitle[y]) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                str = str + "<label><input type='checkbox' id=subtitleCheckbox" + x + " value='" + self.subtitleList[x].id + "' order=" + x + " checked />" + self.subtitleList[x].en + "</label>";
            } else {
                str = str + "<label><input type='checkbox' id=subtitleCheckbox" + x + " value='" + self.subtitleList[x].id + "' order=" + x + " />" + self.subtitleList[x].en + "</label>";
            }
        }

        $("#subtitle_list").html(str);
        self.initSubtitleCheckBox(self, $("#subtitle_list"));
    },

    initDivisionList: function (_self) {
        var self = _self;

        $("#division_list").empty();

        var str = "<select id=\"divisionSelector\">";

        str = str + "<option value=''>--</option>";

        for (var x = 0; x < self.divisionList.length; x++) {

            if (self.divisionList[x].id == self.selected_division) {
                str = str + "<option id=division" + x + " value='" + self.divisionList[x].id + "' order=" + x + " selected >" + self.divisionList[x].en + "</option>";
            } else {
                str = str + "<option id=division" + x + " value='" + self.divisionList[x].id + "' order=" + x + " >" + self.divisionList[x].en + "</option>";
            }
        }
        str = str + "<\select>";
        $("#division_list").html(str);
    },

    initRating: function (_self) {
        var self = _self;
        $("#ratingInput").val(self.rating);
    },

    saveDescriptionLang: function () {
        var editor_lang = CKEDITOR.instances.editor2;
        eval("this.desLangModel." + $("#lang_selectionBox").val() + "= editor_lang.getData()");
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {

        //COMPLETELY UNBIND THE VIEW

        this.selectGuestModel = null;
        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    setCropper: function(){

        $('#imageContainer > img').cropper({
            aspectRatio:   App.movieDetail.posterWidth /  App.movieDetail.posterHeight,
            autoCropArea:1.0 ,
            guides: true,
            highlight: false,
            dragCrop: false,
            movable: false,
            resizable: false
        });
    },
    gotPic: function (event) {

        var self = this;
        if (event.target.files.length == 1 && event.target.files[0].type.indexOf("image/") == 0) {

            App.movieDetail.isNewPoster = true;
            App.imageData = null;

            //get only filename without extension
            var file = event.target.files[0];
            var tempFileComponents = file.type.split("/");
            var fileExtension = tempFileComponents[1];
            App.imageFileName = file.name.substring(0, file.name.length - fileExtension.length - 1);
            App.imageFileName = App.imageFileName.replace(/\s+/g, '');

            console.log('File size =' + file.size);
            console.log('max size =' + self.uploadImageSizeLimit * 1024 * 1024);
            //image file exceed upper limited size, not allow to pass
            if (file.size > self.uploadImageSizeLimit * 1024 * 1024) {
                $("#takePictureField").val('');
                alert("File size is too big. Please upload image less than " + self.uploadImageSizeLimit + "mb.");
                return;
            }  else{
                //allow to remove previous cropped component.
                if(App.imageData) {
                    $('#imageContainer > img').cropper("destroy");
                }
            }

            var url = window.URL ? window.URL : window.webkitURL;

            $('#uploadImage').empty();
            $('#uploadImage').attr('src', url.createObjectURL(event.target.files[0]));
            var canvasImage = document.createElement("img");
            canvasImage.src = url.createObjectURL(event.target.files[0]);

            $("#imageContainer").show();

            canvasImage.onload = function () {
                console.log("Image onLoad fire");
                //change image data to base64
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'),
                    outputFormat = "image/jpeg";

                canvas.height = canvasImage.height;
                canvas.width = canvasImage.width;
                ctx.drawImage(canvasImage, 0, 0);
                App.imageData = canvas.toDataURL(outputFormat);
                //alert("App.imageData = " + App.imageData);
                App.imageData = App.imageData.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

                canvas = null;

                App.movieDetail.setCropper();

            };
        }
        else {
            alert("Please select an image file.");
            $('#uploadImage').val("");
        }
    },

    loadPhoto: function () {
        var self = this;
        if (self.posterFilePath != null && !self.posterFilePath.isEmpty) {
            var srcPath = "/" + App.baseFolder + "/" + self.posterFilePath;
            $('#uploadImage').empty();
            $('#uploadImage').attr('src', srcPath);
            $("#imageContainer").show();
        }
    },

    loadYear: function () {
        var self = this;
        if (self.year != null) {
            yearInput.value = self.year;
        }
    },

    initCheckBoxStyle: function (field) {
        jQuery.fn.multiselect = function () {
            $(this).each(function () {
                var checkboxes = $(this).find("input:checkbox");
                checkboxes.each(function () {
                    var checkbox = $(this);
                    // Highlight pre-selected checkboxes
                    if (checkbox.prop("checked"))
                        checkbox.parent().addClass("multiselect-on");

                    // Highlight checkboxes that the user selects
                    checkbox.click(function () {
                        if (checkbox.prop("checked"))
                            checkbox.parent().addClass("multiselect-on");
                        else
                            checkbox.parent().removeClass("multiselect-on");
                    });
                });
            });
        };

        $(function () {
            field.multiselect();
        });
    },

    initGenreCheckBox: function (_self, field) {
        var self = _self;

        for (var x = 0; x < self.genreList.length; x++) {
            $("#genreCheckbox" + x).click(function () {
                var index = $(this).attr("order");
                var isExist = false;
                var existIndex;


                for (var y = 0; y < self.selected_genre.length; y++) {

                    console.log(self.genreList.length + "|" + index);
                    if (self.selected_genre[y] == self.genreList[index].id) {
                        isExist = true;
                        existIndex = y;
                    }
                }

                if ($(this).is(":checked")) {
                    if (!isExist) {
                        if(self.selected_genre[0] == null){
                            self.selected_genre[0] = self.genreList[index].id
                        } else {
                            self.selected_genre.push(self.genreList[index].id);
                        }
                    }
                } else {
                    if (isExist) {
                        self.selected_genre.splice(existIndex, 1);
                    }
                }
            });
        }
        self.initCheckBoxStyle(field);
    },
    initLanguageCheckBox: function (_self, field) {
        var self = _self;

        for (var x = 0; x < self.languageList.length; x++) {
            $("#languageCheckbox" + x).click(function () {
                var index = $(this).attr("order");
                var isExist = false;
                var existIndex;

                for (var y = 0; y < self.selected_language.length; y++) {
                    if (self.selected_language[y] == self.languageList[index].id) {
                        isExist = true;
                        existIndex = y;
                    }
                }

                if ($(this).is(":checked")) {
                    if (!isExist) {
                        if(self.selected_language[0] == null) {
                            self.selected_language[0] = self.languageList[index].id;
                        } else {
                            self.selected_language.push(self.languageList[index].id);
                        }
                    }
                } else {
                    if (isExist) {
                        self.selected_language.splice(existIndex, 1);
                    }
                }
            });
        }
        self.initCheckBoxStyle(field);
    },
    initSubtitleCheckBox: function (_self, field) {
        var self = _self;

        for (var x = 0; x < self.subtitleList.length; x++) {
            $("#subtitleCheckbox" + x).click(function () {
                var index = $(this).attr("order");
                var isExist = false;
                var existIndex;

                for (var y = 0; y < self.selected_subtitle.length; y++) {
                    if (self.selected_subtitle[y] == self.subtitleList[index].id) {
                        isExist = true;
                        existIndex = y;
                    }
                }

                if ($(this).is(":checked")) {
                    if (!isExist) {
                        if(self.selected_subtitle[0] == null){
                            self.selected_subtitle[0] = self.subtitleList[index].id;
                        } else {
                            self.selected_subtitle.push(self.subtitleList[index].id);
                        }
                    }
                } else {
                    if (isExist) {
                        self.selected_subtitle.splice(existIndex, 1);
                    }
                }
            });
        }
        self.initCheckBoxStyle(field);
    },

    isHide: false
});
