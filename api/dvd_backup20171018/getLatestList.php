<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;
$genre = null;
$lang = "en";

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
} else {
    $lang = 'en';
}


$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT DISTINCT movie_latest.id As id, movie_latest.movieId As movieId, m1.poster As poster, m1.movieTitle As movieTitle            
        FROM movie_latest
        LEFT JOIN (SELECT movies.id As movieId, movies.posterurl As poster, title.movieTitle As movieTitle
                  FROM movies 
                  INNER JOIN 
                        (SELECT movies.titleId AS titleId, movie_dictionary." . $lang . " AS movieTitle 
                         FROM movies 
                         INNER JOIN movie_dictionary
                         ON movies.titleId = movie_dictionary.id) title
                  ON title.titleId = movies.titleId 
                  WHERE movies.isVoid = 0 ) m1
        ON movie_latest.movieId = m1.movieId
        ORDER BY movie_latest.ordering ASC ";

$st = $conn->prepare($sql);
$st->execute();
$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}

$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get latest good', $list);
} else {
    echo returnStatus(0, 'get latest fail');
}


?>
