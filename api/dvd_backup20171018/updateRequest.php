<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$requestId = isset($_POST['requestId']) ? $_POST['requestId'] : null;
$statusId = isset($_POST['statusId']) ? $_POST['statusId'] : null;
$inventoryId = isset($_POST['inventoryId']) ? $_POST['inventoryId'] : null;
$updateBy =  ($_SESSION['email'] != null) ?  $_SESSION['email'] : "customer";

if ($requestId == null || $statusId == null) {
    echo returnStatus(1, 'update movie fail');
} else {

    //setup DB
    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    $conn->exec("set names utf8");

    if ($inventoryId != null) {
        $sql = "UPDATE movie_borrow_history SET inventoryId=:inventoryId, statusId=:statusId,lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE id = " . $requestId . " ";
    } else {
        $sql = "UPDATE movie_borrow_history SET statusId=:statusId,lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE id = " . $requestId . " ";
    }

    //echo $sql;
    $st = $conn->prepare($sql);

    if ($inventoryId != null) {
        $st->bindValue(":inventoryId", $inventoryId, PDO::PARAM_STR);
    }

    $st->bindValue(":statusId", $statusId, PDO::PARAM_STR);
    $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);



    $st->execute();

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {

        if($statusId == 4 || $statusId == 5) {

            $sql = "SELECT inventoryId from movie_borrow_history where id = ".$requestId." ";
            $st = $conn->prepare($sql);
            $st->execute();

            $list = array();
            while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
                $list[] = $row;
            }
            $inventoryId = $list[0]["inventoryId"];

            $sql = "UPDATE movie_inventory SET available=1,lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE id = '".$inventoryId."' ";

            $st = $conn->prepare($sql);
            $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
            $st->execute();

            if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
                echo returnStatus(1, 'update request good');
            } else {
                echo returnStatus(0, 'update request fail');
            }
        } else {
            echo returnStatus(1, 'update request good');
        }

    } else{
        echo returnStatus(0, 'update request fail');
    }

    $conn = null;
}

?>
