<?php

ini_set("display_errors", true);
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$assetId = isset($_POST['assetId']) ? $_POST['assetId'] : '';
$movieId = isset($_POST['movieId']) ? $_POST['movieId'] : '';

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

//*****create Dictionary for Subject
$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare($sql);
$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$uuid = $list[0]["UUID"];

$sql = "INSERT INTO movie_inventory (id, assetId, movieId, available, isVoid, lastUpdate, lastUpdateBy)
          VALUES (:id, :assetId, :movieId, :available, :isVoid, now(), :lastUpdateBy)";
$st = $conn->prepare($sql);
$st->bindValue(":id", $uuid, PDO::PARAM_STR);
$st->bindValue(":assetId", $assetId, PDO::PARAM_STR);
$st->bindValue(":movieId", $movieId, PDO::PARAM_STR);
$st->bindValue(":available", 1, PDO::PARAM_INT);
$st->bindValue(":isVoid", 0, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
$st->execute();

if ($st->rowCount() > 0) {
    echo returnStatus(1, 'Add inventory OK ');
} else {
    echo returnStatus(0, 'Add inventory fail');
}

return 0;

?>
