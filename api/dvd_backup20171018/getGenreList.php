<?php
require( "../../config.php" );

ini_set( "display_errors", true );

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;

if(isset($_REQUEST['getCount'])){
    $getCount = $_REQUEST['getCount'];
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if(!empty($getCount)){
    $sql = "select  count(*) as totalNum from movie_genre where movie_genre.titleId !='' ";
} else {
    $sql = "SELECT  movie_genre.id As id,
                movie_genre.titleId As titleId,
                movie_dictionary.en AS en,
                movie_dictionary.zh_cn AS zh_cn,
                movie_dictionary.fr AS fr,
                movie_dictionary.jp AS jp,
                movie_dictionary.ar AS ar,
                movie_dictionary.es AS es,
                movie_dictionary.de AS de,
                movie_dictionary.ko AS ko,
                movie_dictionary.ru AS ru,
                movie_dictionary.pt AS pt,
                movie_dictionary.zh_hk AS zh_hk,
                movie_dictionary.tr AS tr
            FROM movie_genre 
            INNER JOIN movie_dictionary
            ON movie_genre.titleId = movie_dictionary.id
            ORDER BY movie_genre.ordering ASC";
}


$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get itemList good', $list);
}
else{
    echo returnStatus(0, 'get itemList fail');
}

?>
