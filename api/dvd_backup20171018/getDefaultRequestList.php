<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$date = null;
$status = null;
$room = null;
$lang = "en";
$sqlForFilter = '';


$sqlForFilter = $sqlForFilter . " AND (hist.statusId = 1 OR hist.statusId = 3 OR hist.statusId = 6) ";

if (isset($_REQUEST["lang"])) {
    $lang = $_REQUEST["lang"];
}

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT 
                hist.id As id,
                hist.roomId As room,
                inventory.assetId As assetId,
                moviedetail.movieId As movieId,
                moviedetail.movieTitle As title,
                hist.statusId As statusId,
                hist.requestTime As requesttime, 
                hist.lastUpdate As lastupdate, 
                hist.lastUpdateBy As lastupdateby
            FROM movie_borrow_history hist 
            
            LEFT JOIN 
                (SELECT movies.id As movieId, movie_dictionary." . $lang . " As movieTitle 
                 FROM movies 
                 INNER JOIN movie_dictionary
                 ON movies.titleId = movie_dictionary.id) moviedetail
            ON moviedetail.movieId = hist.movieId
            
            LEFT JOIN (SELECT movie_inventory.id As inventoryId, movie_inventory.assetId As assetId 
                        FROM movie_inventory) inventory
            ON inventory.inventoryId = hist.inventoryId
            
            WHERE enable = 1 " . $sqlForFilter . " 
            ORDER BY
            (case when hist.statusId=1 then 0 
            when hist.statusId=3 then 0 
            when hist.statusId=2 then 1 
            when hist.statusId=6 then 2 
            else 3 end) ASC;";

$st = $conn->prepare($sql);
$st->execute();

$list = array();
while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get default requestList good', $list);
} else {
    echo returnStatus(0, 'get default requestList fail');
}
?>
