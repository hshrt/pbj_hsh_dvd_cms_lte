<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$movieId = null;

if (isset($_REQUEST['movieId'])) {
    $movieId = $_REQUEST['movieId'];
}

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

if (isset($_REQUEST['onlyavailable'])) {

    $sql = "SELECT movie_inventory.id As id, movie_inventory.assetId As assetId
                FROM movie_inventory
                WHERE movie_inventory.isVoid = 0 
                      AND movie_inventory.available = 1 
                      AND movie_inventory.movieId = '" . $movieId . "' ORDER BY movie_inventory.assetId ASC";

    $st = $conn->prepare($sql);

    $st->execute();

    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    $conn = null;

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get onlyavailable inventoryList good', $list);
    } else {
        echo returnStatus(0, 'get onlyavailable inventoryList fail');
    }

} else {

    $sql = "SELECT movie_inventory.id As id, movie_inventory.assetId As assetId, 
            (CASE WHEN hist.roomId IS NULL THEN '-' 
                  WHEN movie_inventory.available = 1 THEN '-'
                  ELSE hist.roomId END) As roomId, 
                  hist.requestTime As requestTime, 
                (CASE WHEN movie_inventory.available = 1 THEN 0 ELSE hist.statusId END) As statusId
                FROM movie_inventory
                
                LEFT JOIN (SELECT 
                            movie_borrow_history.inventoryId As inventoryId,
                            movie_borrow_history.roomId As roomId,
                            movie_borrow_history.requestTime As requestTime,
                            movie_borrow_history.statusId As statusId
                          FROM movie_borrow_history 
                          WHERE movie_borrow_history.statusId = 2 OR movie_borrow_history.statusId = 3 OR movie_borrow_history.statusId = 12 OR movie_borrow_history.statusId = 13) hist
                ON hist.inventoryId = movie_inventory.id
                
                WHERE movie_inventory.isVoid = 0 
                      AND movie_inventory.movieId = '" . $movieId . "' ORDER BY movie_inventory.assetId ASC; ";

    $st = $conn->prepare($sql);

    $st->execute();

    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get inventoryList good', $list);
    } else {
        echo returnStatus(0, 'get inventoryList fail');
    }
    $conn = null;
}

?>
