<?php

require("../../config.php");
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
include("../checkSession.php");

$movieId = $_POST["id"];

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "UPDATE movies SET isVoid=1, lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE id = :movieId;";

//echo $sql;

$st = $conn->prepare($sql);
$st->bindValue(":movieId", $movieId, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);

$st->execute();


$sql = "DELETE FROM movies_genre WHERE movieId = '" .$movieId."'" ;
$st = $conn->prepare($sql);
$st->execute();

$sql = "DELETE FROM movies_language WHERE movieId = '" .$movieId."'" ;
$st = $conn->prepare($sql);
$st->execute();

$sql = "DELETE FROM movies_subtitle WHERE movieId = '" .$movieId."'" ;
$st = $conn->prepare($sql);
$st->execute();


$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'delete movie good');
}
else{
    echo returnStatus(0 , 'delete movie fail');
}

?>
